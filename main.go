package main

import "fmt"

func main() {
	// Create an indent
	if true {
		err := someThing()
		if err != nil {
			fmt.Println("oh noes")
		}
	}
}

func someThing() error {
	return nil
}
